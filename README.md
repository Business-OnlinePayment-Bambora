# Business::OnlinePayment::Bambora

## Building

```
perl Makefile.PL
make
make install
```

## Testing

```
export BAMBORA_MERCHANT_ID=1234567890
export BAMBORA_API_KEY=XOXOXOXOXOXOXOX
make test
```

## Usage Documentation

`perldoc Business::OnlinePayment::Bambora`

## See Also

- [Git Repo](http://git.freeside.biz/gitweb/?p=Business-OnlinePayment-Bambora.git)
