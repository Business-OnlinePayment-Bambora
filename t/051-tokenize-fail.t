#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;

use lib 't';
use TestFixtures;
use Business::OnlinePayment;
use Data::Dumper;
    $Data::Dumper::Sortkeys = 1;
    $Data::Dumper::Indent = 1;

my $merchant_id = $ENV{BAMBORA_MERCHANT_ID};
my $api_key     = $ENV{BAMBORA_API_KEY};

SKIP: {
  skip 'Missing env vars BAMBORA_MERCHANT_ID and BAMBORA_API_KEY', 36
    unless $merchant_id && $api_key;

  #
  # Attempt charge with a normal credit card number as card_token
  # Expect fail
  #

  my %content_fail = (
    login => $merchant_id,
    password => $api_key,

    action => 'Normal Authorization',
    card_token => '4242424242424242',
    amount => '24.95',
  );

  my ( $tr_fail, $response_fail ) = make_api_request( \%content_fail );

  inspect_transaction(
    $tr_fail,
    { is_success => 0 },
    [qw/ error_message /],
  );

  ok( $tr_fail->error_message =~ /invalid card_token/i,
      'Saw expected error_message'
  );
}

done_testing;