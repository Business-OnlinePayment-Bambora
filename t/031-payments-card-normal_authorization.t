#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;

use lib 't';
use TestFixtures;
use Business::OnlinePayment;

my $merchant_id = $ENV{BAMBORA_MERCHANT_ID};
my $api_key     = $ENV{BAMBORA_API_KEY};

SKIP: {
  skip 'Missing env vars BAMBORA_MERCHANT_ID and BAMBORA_API_KEY', 78
    unless $merchant_id && $api_key;

  my %content = (
    common_content(),

    login => $merchant_id,
    password => $api_key,

    action => 'Normal Authorization',
  );

  # Test approved card numbers,
  # ref: https://dev.na.bambora.com/docs/references/payment_APIs/test_cards/
  my %approved_cards = (
    visa        => { card => '4030000010001234', cvv2 => '123' },
    mastercard  => { card => '5100000010001004', cvv2 => '123' },
    mastercard2 => { card => '2223000048400011', cvv2 => '123' },
    amex        => { card => '371100001000131',  cvv2 => '1234' },
    visa        => { card => '4030000010001234', cvv2 => '123' },
    discover    => { card => '6011500080009080', cvv2 => '123' },
  );

  for my $name ( keys %approved_cards ) {
    $content{card_number} = $approved_cards{$name}->{card};
    $content{cvv2} = $approved_cards{$name}->{cvv2};

    my ( $tr, $response ) = make_api_request( \%content );

    inspect_response(
      $response,
      {
        amount => '9.99',
        approved => 1,
        auth_code => 'TEST',
        message => 'Approved',
        message_id => 1,
        payment_method => 'CC',
        type => 'P',
      },
      [qw/
        card
        created
        order_number
        risk_score
        id
      /],
    );

    inspect_transaction(
      $tr,
      {
        is_success => 1,
        authorization => 'TEST',
      },
      [qw/
        message_id
        order_number
        txn_date
        avs_code
      /],
    );

  }

  # Test declined card numbers,
  # ref: https://dev.na.bambora.com/docs/references/payment_APIs/test_cards/
  my %decline_cards = (
    visa        => { card => '4003050500040005', cvv2 => '123' },
    mastercard  => { card => '5100000020002000', cvv2 => '123' },
    amex        => { card => '342400001000180', cvv2 => '1234' },
    discover    => { card => '6011000900901111', cvv2 => '123' },
  );
  for my $name ( keys %decline_cards ) {
    $content{card_number} = $decline_cards{$name}->{card};
    $content{cvv2} = $decline_cards{$name}->{cvv2};

    my ( $tr, $response ) = make_api_request( \%content );

    inspect_transaction(
      $tr,
      {
        is_success => 0,
      },
      [qw/
        error_message
      /],
    );
    ok( $tr->result_code != 1, '$tr->result_code != 1' );
  }
}

done_testing;