#!/usr/bin/env perl
use strict;
use warnings;
use Test::More tests => 1;

BEGIN{ use_ok( 'Business::OnlinePayment::Bambora' ) }

done_testing();
