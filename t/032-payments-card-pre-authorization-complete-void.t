#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;

use lib 't';
use TestFixtures;
use Business::OnlinePayment;

my $merchant_id = $ENV{BAMBORA_MERCHANT_ID};
my $api_key     = $ENV{BAMBORA_API_KEY};

SKIP: {
  skip 'Missing env vars BAMBORA_MERCHANT_ID and BAMBORA_API_KEY', 56
    unless $merchant_id && $api_key;

  my %content = (
    common_content(),

    login => $merchant_id,
    password => $api_key,

    action => 'Authorization Only',
  );

  #
  # Process a pre-auth
  #

  my ( $tr, $response ) = make_api_request( \%content );

  inspect_response(
    $response,
    {
      amount => '9.99',
      approved => 1,
      auth_code => 'TEST',
      message => 'Approved',
      message_id => 1,
      payment_method => 'CC',
      type => 'PA',
    },
    [qw/
      card
      created
      order_number
      risk_score
      id
     /],
  );

  inspect_transaction(
    $tr,
    {
      is_success => 1,
    },
    [qw/
      message_id
      authorization
      order_number
      txn_date
      avs_code
    /],
  );

  #
  # Process a post-auth
  #

  my %content_pa = (
    %content,
    action => 'Post Authorization',
    order_number => $tr->order_number,
    amount => '8.99', # $1 Less than pre-auth
  );

  my ( $tr_pa, $response_pa ) = make_api_request( \%content_pa );

  inspect_response(
    $response_pa,
    {
      amount => '8.99',
      approved => '1',
      message => 'Approved',
      message_id => '1',
      type => 'PAC',
    },
    [qw/
      authorizing_merchant_id
      card
      created
      order_number
      id
    /],
  );

  inspect_transaction(
    $tr_pa,
    {
      is_success => 1,
    },
    [qw/
      message_id
      authorization
      order_number
      txn_date
      avs_code
    /],
  );

  #
  # Void Transaction
  #

  my %content_void = (
    action => 'Void',
    login => $content{login},
    password => $content{password},

    order_number => $tr_pa->order_number,
    amount => '8.99',
  );

  my ( $tr_void, $response_void ) = make_api_request( \%content_void );

  inspect_response(
    $response_void,
    {
      amount => '8.99',
      approved => '1',
      message => 'Approved',
      message_id => '1',
      type => 'R',
    },
    [qw/
      authorizing_merchant_id
      card
      created
      order_number
      id
    /],
  );

  inspect_transaction(
    $tr_void,
    { is_success => 1 },
    [],
  );

}

done_testing;