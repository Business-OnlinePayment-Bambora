#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;

use lib 't';
use Business::OnlinePayment;
use Business::CreditCard qw/ validate /;

my $tr;
ok( $tr = Business::OnlinePayment->new('Bambora'), 'Instantiatiate $tr' );

my $token;
ok( $token = $tr->generate_token, "\$tr->generate_token: $token" );
ok( $token =~ /^99\d{14}$/, 'Token matches expected format' );

#
# Generate 1,000,000 tokens in series, and check for token collissions
#

my $start = time();
my $invalid = 0;
my %seen;
for my $i ( 1..1000000 ) {
    my $token = Business::OnlinePayment::Bambora->generate_token;
    $invalid++ unless validate( $token );
    $seen{$token}++;
}
my $end = time();
ok(
    scalar keys %seen == 1000000,
    sprintf 'Generated %s tokens in %s seconds', scalar(keys(%seen)), $end-$start
);
ok( $invalid == 0, 'All generated tokens pass Luhn check' );
ok( ! grep( {$_ > 1} values %seen ), "All 1M tokens were unique" );

done_testing;