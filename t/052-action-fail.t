#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;

use lib 't';
use TestFixtures;
use Business::OnlinePayment;
use Data::Dumper;
    $Data::Dumper::Sortkeys = 1;
    $Data::Dumper::Indent = 1;

my $merchant_id = $ENV{BAMBORA_MERCHANT_ID};
my $api_key     = $ENV{BAMBORA_API_KEY};

SKIP: {
  skip 'Missing env vars BAMBORA_MERCHANT_ID and BAMBORA_API_KEY', 6
    unless $merchant_id && $api_key;

  #
  # Attempt with invalid action name
  # Expect fail
  #

  my %content_fail = (
    common_content(),

    login => $merchant_id,
    password => $api_key,

    action => 'Norml Authorzatin',
    amount => '24.95',
  );

  my ( $tr_fail, $response_fail ) = make_api_request( \%content_fail );

  inspect_transaction(
    $tr_fail,
    { is_success => 0 },
    [qw/ error_message /],
  );

  ok( $tr_fail->error_message =~ /action is unsupported/i,
      'Saw expected error_message'
  );
}

done_testing;