#!/usr/bin/env perl
use strict;
use warnings;
use Test::More;

use lib 't';
use TestFixtures;
use Business::OnlinePayment;
use Data::Dumper;
    $Data::Dumper::Sortkeys = 1;
    $Data::Dumper::Indent = 1;

my $merchant_id = $ENV{BAMBORA_MERCHANT_ID};
my $api_key     = $ENV{BAMBORA_API_KEY};

SKIP: {
  skip 'Missing env vars BAMBORA_MERCHANT_ID and BAMBORA_API_KEY', 36
    unless $merchant_id && $api_key;

  my %content = (
    common_content(),

    login => $merchant_id,
    password => $api_key,

    action => 'Tokenize',
  );

  #
  # Create a payment profile with Tokenize
  #

  my ( $tr, $response ) = make_api_request( \%content );

  inspect_response(
    $response,
    {
      code => 1,
      message => 'Operation Successful',
    },
    [qw/ customer_code /],
  );

  ok(
    $response->{customer_code} eq $tr->card_token,
    '$tr->card_token eq $response->{customer_code}'
  );

  #
  # Create a charge against the payment profile
  # with the token set as 'card_number'
  #

  my %content_ch1 = (
    %content,
    action => 'Normal Authorization',
    card_number => $tr->card_token,
    amount => '2.95',
  );

  my ( $tr_ch1, $response_ch1 ) = make_api_request( \%content_ch1 );

  # warn Dumper({
  #   response_ch1 => $response_ch1,
  # });

  inspect_response(
    $response_ch1,
    {
      amount => $content_ch1{amount},
      approved => 1,
      auth_code => 'TEST',
      authorizing_merchant_id => $content{login},
      message => 'Approved',
      payment_method => 'CC',
      type => 'P',
    },
    [qw/
      card
      created
      order_number
    /],
  );


  #
  # Create a charge against the payment profile
  # with the token set as 'card_token'
  #

  my %content_ch2 = (
    login => $content{login},
    password => $content{password},
    action => 'Normal Authorization',
    #card_token => '9915559773829941',
    card_token => $tr->card_token,
    amount => '7.77',
  );

  my ( $tr_ch2, $response_ch2 ) = make_api_request( \%content_ch2 );

  # warn Dumper({
  #   response_chs => $response_ch2
  # });

  inspect_response(
    $response_ch2,
    {
      amount => $content_ch2{amount},
      approved => 1,
      auth_code => 'TEST',
      authorizing_merchant_id => $content{login},
      message => 'Approved',
      payment_method => 'CC',
      type => 'P',
    },
    [qw/
      card
      created
      order_number
    /],
  );

  #
  # Attempt charge with a normal credit card number as card_token
  # Expect fail
  #

  my %content_fail = (
    %content_ch2,
    card_token => '4242424242424242',
    amount => '24.95',
  );

  my ( $tr_fail, $response_fail ) = make_api_request( \%content_fail );

  inspect_transaction(
    $tr_fail,
    { is_success => 0 },
    [qw/ error_message /],
  );

}

done_testing;