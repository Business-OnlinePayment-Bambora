use ExtUtils::MakeMaker;

WriteMakefile(
  NAME => 'Business::OnlinePayment::Bambora',
  AUTHOR => 'Mitch Jackson <mitch@freeside.biz>',
  LICENSE => 'agpl_3',
  VERSION_FROM => 'lib/Business/OnlinePayment/Bambora.pm',
  PREREQ_PM => {qw/
    Business::OnlinePayment 0
    Carp 0
    Cpanel::JSON::XS 0
    Data::Dumper 0
    LWP::UserAgent 0
    MIME::Base64 0
    Net::HTTPS 6
    Net::SSL 0
    Time::HiRes 0
    Unicode::Truncate 0
    URI::Escape 0
  /},
  TEST_REQUIRES => {qw/
    Test::More 0
    Business::CreditCard 0
  /}
);
